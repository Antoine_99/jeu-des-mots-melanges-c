#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include "math.h"

using namespace std;


int main()
{

    string motJoueur1 , motJoueur2 , motMelange;
    int nbEssais = 5;
    srand(time(0));

    cout << "Bonjour, bienvenue au jeux des mots melanges !" << endl << endl;
    cout << "Choississez un mot : ";
    cin >> motJoueur1;
    cout << endl;
    motMelange = melangerMot(motJoueur1);

    do
   {
      cout << "Vous avez " << nbEssais << " essais." << endl << endl;
      cout << endl << "Quel est ce mot ? " << motMelange << endl;
      cin >> motJoueur2;

      if (motJoueur2 == motJoueur1)
      {
         cout << "Bravo vous avez trouver le bon mot !" << endl;
      }
      else
      {
         cout << "Ce n'est pas le bon mot." << endl << endl;
         nbEssais --;
      }

      if (nbEssais == 0)
      {
          cout << "Vous n'avez plus d'essais." << endl;
          cout << "La bonne response est " << motJoueur1 << endl;
          cout << "La partie est terminee." << endl;

      }
   }while (motJoueur2 != motJoueur1 && nbEssais != 0);

    return 0;
}


