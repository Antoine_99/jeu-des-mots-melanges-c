#include "math.h"
#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>

using namespace std;

string melangerMot(string mot)
{
    string motMelange;
   int position;

   while (mot.size() != 0)
   {
      position = rand() % mot.size();
      motMelange += mot[position];
      mot.erase(position, 1);
    }

   return motMelange;
};
